# flutter_boom_app

#### 介绍
一个flutter开发的社群教育类的app

#### 停止更新

#### 关于手机号隐私问题

- 现禁用真实发送验证码接口。
- 注册时使用正常格式的手机号码,提交之前先点击获取验证码后再进行注册或者登录以及修改密码 默认验证码为000000

#### 软件架构

包括了用户、文章系统、课程观看、职业推荐等模块。
还有一些模块没有完全完善。可以供学习使用。

包括android和ios版本 ios版本因为我的网络问题现在无法正常打包。有兴趣的童鞋可以自行打包运行。

#### 一些页面

|  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0416/121117_1878cb8d_453125.jpeg "WechatIMG21.jpeg")  |  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0416/120739_9eb39e58_453125.jpeg "WechatIMG22.jpeg")  |  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0416/120828_79e48a45_453125.jpeg "WechatIMG23.jpeg")  |
| --- | --- | --- |
|  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0416/120853_d337ad95_453125.jpeg "WechatIMG24.jpeg")  |  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0417/092226_2f4277f8_453125.jpeg "WechatIMG16.jpeg")  |  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0416/120917_0dd8952f_453125.jpeg "WechatIMG26.jpeg")  |
|  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0416/120929_74a13965_453125.jpeg "WechatIMG27.jpeg")  |  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0416/120940_3a86e9b3_453125.jpeg "WechatIMG28.jpeg")  |  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0417/092308_63c7cfb9_453125.jpeg "WechatIMG14.jpeg")  |

#### 安装教程

Android版本

![下载二维码](https://images.gitee.com/uploads/images/2020/0416/121439_6bbbcb5d_453125.jpeg "1587010435770.jpg")

密码：123456

#### 参与贡献

- 文景睿
- 大黄
- 胡老
- 网络框架dio包借鉴了[flutter_deer](https://gitee.com/49849668/flutter_deer)

#### 近期计划

- 应用内升级
- 个人中心的文章和动态列表
- 文章系统的积分显示
- 优化动态查看大图的操作
- 增加html页面的链接跳转和图片查看
